import React from "react"

const Table = (props) => {
    let table = [<tr><th></th><th>words</th><th>count</th></tr>]
    let keys = Object.keys(props.words)
    let values = Object.values(props.words)

    for(let i = 0; i < keys.length; i++){
	    table.push(<tr><th>{i+1}</th><th>{keys[i]}</th><th>{values[i]}</th></tr>)
    }
	return ( <table><tbody>{table}</tbody></table> )	
}

export default Table