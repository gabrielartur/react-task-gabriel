import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import Hello from './Hello';
import {Route, BrowserRouter as Router} from 'react-router-dom';
import * as serviceWorker from './serviceWorker';

const routing = (
	<Router>
	   <div>
		<Route exact path="/" component={Hello} />
		<Route path="/stats" component={App} />
	   </div>	
	</Router>
)


ReactDOM.render(routing, document.getElementById('root'));

// unregister() or register()
serviceWorker.register();
