import React, {Component} from 'react';
import Table from "./Table"
import './App.css';

class App extends Component {
  constructor(){
    super()
    this.state = {
      authors: {},
      selectedAuthors: [],
      words: {}
    }
    this.handleChange = this.handleChange.bind(this)
  }

  componentDidMount(){
    fetch('/authors')
      .then(response => response.json())
      .then(data => this.setState({authors: data}))	
  }

  handleChange(ev){
    let url = ev.target.value
    if(url === "All"){
	  fetch(`/stats`)
	      .then(response => response.json())
      	.then(data => this.setState({words: data}))
	
	  this.setState({selectedAuthors: ev.target.value})
    } else {
    let authors = this.state.selectedAuthors
    if(authors.includes(ev.target.value)){  
        alert("You already selected this author")
        return
      }
    if(authors.length > 0 && !authors.includes("All")){	
    	fetch(`/stats/${url}`)
      	  .then(response => response.json())
      	  .then(data => {
		          let currKeys = Object.keys(data)
		          for(let i = 0; i < currKeys.length; i++){
			        data[currKeys[i]] += this.state.words[currKeys[i]]
              }
              this.setState({words: data}) 
          })
          this.setState({selectedAuthors: authors.concat(" " + ev.target.value)})
    } else {
      fetch(`/stats/${url}`)
	      .then(response => response.json())
      	.then(data => this.setState({words: data}))
	
	      this.setState({selectedAuthors: ev.target.value})
    }		
  }
}

  render(){
    let options = [<option key={0} value="All">All</option>]
    let keys = Object.keys(this.state.authors)
    let values = Object.values(this.state.authors)

    for(let i = 0; i < keys.length; i++){
	    options.push(<option label={values[i]} key={keys[i]} value={keys[i]}>{values[i]}</option>)
    }

    let displayedData = ""
    if(this.state.selectedAuthors.length > 0){
      displayedData = (<div><h3>Selected author(tag): {this.state.selectedAuthors}</h3>
      <Table words={this.state.words} /></div>)
    }

    return (
      <div>
	      <h3>Select an author from list below:</h3>
	      <br/>
        <select onChange={this.handleChange} id="select">
	      {options}
	      </select>
	      {displayedData}
      </div>
    )
  }
}

export default App;
